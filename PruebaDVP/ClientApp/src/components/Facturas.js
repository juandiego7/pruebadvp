﻿
import React, { useState, useEffect } from 'react';
import { clientesService } from "../services/clientes.service";
import { productosService } from "../services/productos.service";
import { facturasService } from "../services/facturas.service";
import Utils from "../generic/Utils";

import {
    Button,
    TextField,
    Typography,
    Container,
    Grid,
    Select,
    InputLabel,
    MenuItem,
    FormControl,
    Card,
    TableContainer,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Paper,
    FormHelperText,
    Snackbar,
    IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';


const Facturas = () => {
    const [clientesItems, setClientesItems] = useState([]);
    const [productos, setProductos] = useState([]);
    const [cliente, setCliente] = useState({ id: '', razonSocial:'' });
    const [numeroFactura, setNumeroFactura] = useState('');
    const [data, setData] = useState([]);
    const [newRow, setNewRow] = useState({ id: '', nombreProducto: '', imagenProducto: '', precioUnitario: '', cantidad: '' });
    const [subtotal, setSubtotal] = useState(0);
    const [errorCliente, setErrorCliente] = useState('')
    const [errorNumeroFactura, setErrorNumeroFactura] = useState('') 
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [mensajeSnackbar, setMensajeSnackbar] = useState('');

    const onClearData = () => {
        setData([])
        setNumeroFactura('')
        setCliente({ id: '', razonSocial: '' })
        setSubtotal(0)
        setErrorCliente('')
        setErrorNumeroFactura('')
    }

    const onChangeCliente = (id) => {
        var cliente = clientesItems.find(c => c.id === id)
        if (!Utils.isEmpty(cliente) && !Utils.isEmpty(errorCliente)) {
            setErrorCliente('')
        }   
        setCliente(cliente || { id: '', razonSocial: '' })
    }

    const onChangeNumeroFactura = (number) => {
        if (!Utils.isEmpty(number) && !Utils.isEmpty(errorNumeroFactura)) {
            setErrorNumeroFactura('')
        }
        setNumeroFactura(number)              
    }

    const handleRowChange = (index, e) => {
        const { name, value } = e.target;
        const updatedData = data.map((item, i) =>
            i === index ? { ...item, [name]: value } : item
        );
        setData(updatedData);
        calculateSubtotal(updatedData);          
    };
   
    const handleAddRow = () => {
        setData([...data, newRow]);
    };

    const handleSaveFactura = () => {
        if (Utils.isEmpty(cliente.id)) {
            setErrorCliente("Debe seleccionar un cliente");
            return;
        }
        if (Utils.isEmpty(numeroFactura)) {
            setErrorNumeroFactura("Debe ingresar un número de factura");
            return;
        }
        const factura = {
            id: 0,
            idCliente: cliente.id,
            numeroFactura: numeroFactura,
            numeroTotalArticulos: data.reduce((total, row) => total + parseFloat(row.cantidad), 0),
            subTotalFactura: subtotal,
            totalImpuesto: calculateImpuestos(),
            totalFactura: calculateTotal(),
            detallesFactura: data.map(d =>{
                return {
                    id: 0,
                    idFactura: 0,
                    idProducto: d.id,
                    cantidadDeProducto: d.cantidad,
                    precioUnitarioProducto: d.precioUnitario,
                    subTotalProducto: d.cantidad * d.precioUnitario
                }
            })
        }
        facturasService.createFactura(factura).then(
            response => {
                if (response.succeeded) {
                    setMensajeSnackbar('Datos guardados exitosamente');                    
                    onClearData()
                } else {
                    setMensajeSnackbar('Error: ' + response.message);                    
                }
                setOpenSnackbar(true);
            },
            error => {
                console.log("facturasService error", error)
            }
        );
    };

    const buscarClientesItems = () => {
        clientesService.getClientesItems().then(
            response => {
                if (response.succeeded) {
                    setClientesItems(response.data)
                }
            },
            error => {
                console.log("error", error)
            }
        );
    }

    const buscarProductos = () => {
        productosService.getProductos().then(
            response => {
                if (response.succeeded) {
                    setProductos(response.data)
                }
            },
            error => {
                console.log("productos error", error)
            }
        );
    }

    const setProductoRow = (id, index) => {
        const productoSelected = productos.find(p => p.id === id)
        const detalleFactura = data[index]
        var producto = {
            id: productoSelected.id,
            nombreProducto: productoSelected.nombreProducto,
            precioUnitario: productoSelected.precioUnitario,
            imagenProducto: 'data:image/jpeg;base64,'+productoSelected.imagenProducto
        }
        const updatedData = data.map((item, i) =>
            i === index ? { ...item,...producto } : item
        );
        setData(updatedData);
        calculateSubtotal(updatedData);          
    }

    const handleCloseSnackbar = () => {        
        setOpenSnackbar(false);
    };

    const calculateSubtotal = (data) => {
        const subtotal = data.reduce((total, row) => total + row.cantidad * row.precioUnitario, 0);
        setSubtotal(subtotal);
    };

    const calculateImpuestos = () => {
        return subtotal * 0.19;
    }

    const calculateTotal = () => {
        return subtotal + calculateImpuestos()
    }

    useEffect(() => {
        buscarClientesItems();
        buscarProductos();
    }, []); 

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={openSnackbar}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                message={mensajeSnackbar}
                action={
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                }
            />
            <Card style={{ padding: 20, backgroundColor: '#cfe8fc' }}>
                <Container>
                    <Container style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                        <Typography variant="body1">Nueva factura</Typography>
                        <Button variant="contained" color="default" onClick={onClearData}>
                            Nuevo
                        </Button>
                    </Container>
                    <Container>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={3}>
                                <FormControl
                                    fullWidth
                                    error={!Utils.isEmpty(errorCliente)}>
                                    <InputLabel id="select-cliente-label">Cliente</InputLabel>
                                    <Select
                                        labelId="select-cliente-label"
                                        id="demo-simple-select"
                                        value={cliente.id}
                                        onChange={(e) => onChangeCliente(e.target.value)}
                                    >
                                        <MenuItem value="">
                                            <em>Selecciona una opción</em>
                                        </MenuItem>
                                        {clientesItems.map((cliente) => (
                                            <MenuItem key={cliente.id} value={cliente.id}>{cliente.razonSocial}</MenuItem>                                 
                                        ))}
                                    </Select>
                                    <FormHelperText>{errorCliente}</FormHelperText>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField
                                    fullWidth
                                    label="Número de Factura"
                                    name="NumeroFactura"
                                    type="number"
                                    helperText={errorNumeroFactura}
                                    error={!Utils.isEmpty(errorNumeroFactura)}
                                    value={numeroFactura}
                                    onChange={(e) => onChangeNumeroFactura(e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={12}>
                                <Button type="button" variant="contained" color="primary" onClick={handleAddRow}>
                                    Agregar producto
                                </Button>
                            </Grid>
                            <Grid item xs={12}>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Producto</TableCell>
                                                <TableCell>Precio Unitario</TableCell>
                                                <TableCell>Cantidad</TableCell>
                                                <TableCell>Imagen</TableCell>
                                                <TableCell>Totales</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {data.map((row, index) => (
                                                <TableRow key={index}>                                                                                                                                                                                                                      
                                                    <TableCell>
                                                        <Select
                                                            fullWidth
                                                            id="demo-simple-select"
                                                            value={row.id}
                                                            onChange={(e) => setProductoRow(e.target.value, index)}
                                                        >
                                                            {productos.map((producto) => (
                                                                <MenuItem key={producto.id} value={producto.id}>
                                                                    {producto.nombreProducto}
                                                                </MenuItem>
                                                            ))}
                                                        </Select>
                                                    </TableCell>
                                                    <TableCell>{Utils.formatNumber(row.precioUnitario)}</TableCell>
                                                    <TableCell>
                                                        <TextField
                                                            name="cantidad"
                                                            type="number"
                                                            value={row.cantidad}
                                                            onChange={(e)=>handleRowChange(index, e)}
                                                        />
                                                    </TableCell>                                                                                                      
                                                    <TableCell>
                                                        {row.imagenProducto && <img src={row.imagenProducto} alt="Imagen" style={{ width: 50, height: 50 }} />}
                                                    </TableCell>
                                                    <TableCell>{Utils.formatNumber(row.cantidad * row.precioUnitario)}</TableCell>
                                                </TableRow>
                                            ))}
                                            {data.length > 0 &&
                                                <>
                                                <TableRow>
                                                    <TableCell colSpan={3}></TableCell>
                                                    <TableCell>Sub total</TableCell>
                                                    <TableCell >{Utils.formatNumber(subtotal)}</TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell colSpan={3}></TableCell>
                                                    <TableCell>Impuestos (19%)</TableCell>
                                                    <TableCell>{Utils.formatNumber(calculateImpuestos())}</TableCell>
                                                </TableRow>
                                                <TableRow>
                                                    <TableCell colSpan={3}></TableCell>
                                                    <TableCell>Total</TableCell>
                                                    <TableCell>{Utils.formatNumber(calculateTotal())}</TableCell>
                                                </TableRow>
                                                </>
                                            }                                                                                       
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>
                            {data.length > 0 &&
                                <Grid item xs={12}>
                                    <Button type="button" variant="contained" style={{ backgroundColor: 'green', color: 'white' }} onClick={handleSaveFactura}>
                                        Guardar
                                    </Button>
                                </Grid>
                            }                                
                        </Grid>
                    </Container>
                </Container>
            </Card>
        </div>
    );
}

export default Facturas;
