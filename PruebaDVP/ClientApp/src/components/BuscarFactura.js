﻿

import React, { useState, useEffect } from 'react';
import { facturasService } from "../services/facturas.service";
import { clientesService } from "../services/clientes.service";
import Utils from "../generic/Utils";

import {
    Button,
    TextField,
    Typography,
    Container,
    Grid,
    Select,
    InputLabel,
    MenuItem,
    FormControl,
    FormControlLabel,
    FormLabel,
    RadioGroup,
    Radio,
    Card,
    TableContainer,
    Table,
    TableBody,
    TableCell,
    TableHead,
    TableRow,
    Paper,
    FormHelperText,
    Snackbar,
    IconButton
} from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';


const BuscarFactura = () => {
    const [clientesItems, setClientesItems] = useState([]);
    const [facturas, setFacturas] = useState([]);
    const [cliente, setCliente] = useState({ id: '', razonSocial: '' });
    const [numeroFactura, setNumeroFactura] = useState('');
    const [data, setData] = useState([]);
    const [newRow, setNewRow] = useState({ id: '', nombreProducto: '', imagenProducto: '', precioUnitario: '', cantidad: '' });
    const [subtotal, setSubtotal] = useState(0);
    const [errorCliente, setErrorCliente] = useState('')
    const [errorNumeroFactura, setErrorNumeroFactura] = useState('')
    const [openSnackbar, setOpenSnackbar] = useState(false);
    const [mensajeSnackbar, setMensajeSnackbar] = useState('');
    const [tipoBusqueda, setTipoBusqueda] = React.useState('Cliente');

    const getFacturasPorCliente = (idCliente) => {
        facturasService.getFacturasPorCliente(idCliente).then(
            response => {
                if (response.succeeded) {
                    setFacturas(response.data)
                    if (response.data.length === 0) {
                        setMensajeSnackbar('No hay facturas asociadas a ese cliente');
                        setOpenSnackbar(true);
                    }
                }
            },
            error => {
                console.log("error", error)
            }
        );
    }

    const getFacturasPorNumeroFactura = (numeroFactura) => {
        facturasService.getFacturasPorNumeroFactura(numeroFactura).then(
            response => {
                if (response.succeeded) {
                    setFacturas(response.data)
                    if (response.data.length === 0) {
                        setMensajeSnackbar('No hay facturas asociadas a ese número de factura');                    
                        setOpenSnackbar(true);
                    }
                }
            },
            error => {
                console.log("error", error)
            }
        );
    }

    const handleSearch = () => {
        if (tipoBusqueda === 'Cliente') {
            if (Utils.isEmpty(cliente.id)) {
                setErrorCliente("Debe seleccionar un cliente");
                return;
            }
            getFacturasPorCliente(cliente.id)
        } else {
            if (Utils.isEmpty(numeroFactura)) {
                setErrorNumeroFactura("Debe ingresar un número de factura");
                return;
            }
            getFacturasPorNumeroFactura(numeroFactura)
        }
    }

    const onChangeCliente = (id) => {
        var cliente = clientesItems.find(c => c.id === id)
        if (!Utils.isEmpty(cliente) && !Utils.isEmpty(errorCliente)) {
            setErrorCliente('')
        }
        setCliente(cliente || { id: '', razonSocial: '' })
    }

    const onChangeNumeroFactura = (number) => {
        if (!Utils.isEmpty(number) && !Utils.isEmpty(errorNumeroFactura)) {
            setErrorNumeroFactura('')
        }
        setNumeroFactura(number)
    }

    const buscarClientesItems = () => {
        clientesService.getClientesItems().then(
            response => {
                if (response.succeeded) {
                    setClientesItems(response.data)
                }
            },
            error => {
                console.log("error", error)
            }
        );
    }

    const handleCloseSnackbar = () => {
        setOpenSnackbar(false);
    };
      
    useEffect(() => {
        buscarClientesItems();
    }, []);

    return (
        <div>
            <Snackbar
                anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
                open={openSnackbar}
                autoHideDuration={6000}
                onClose={handleCloseSnackbar}
                message={mensajeSnackbar}
                action={
                    <IconButton size="small" aria-label="close" color="inherit" onClick={handleCloseSnackbar}>
                        <CloseIcon fontSize="small" />
                    </IconButton>
                }
            />
            <Card style={{ padding: 20, backgroundColor: '#cfe8fc' }}>
                <Container>
                    <Container style={{ display: 'flex', flexDirection: 'row', alignItems: 'center', gap: '10px', marginBottom: '20px' }}>
                        <Typography variant="body1">Tipo de Busqueda</Typography>
                        <FormControl component="fieldset">
                            <RadioGroup
                                row
                                value={tipoBusqueda} onChange={(e) => setTipoBusqueda(e.target.value)}>
                                <FormControlLabel                                   
                                    value="Cliente"
                                    control={<Radio color="primary" />}
                                    label={<span style={{ fontWeight: 'bold' }}>Cliente</span> }
                                    labelPlacement="top"
                                />
                                <FormControlLabel
                                    value="NumeroFactura"
                                    control={<Radio color="primary" />}
                                    label={<span style={{ fontWeight: 'bold' }}>Número Factura</span>}
                                    labelPlacement="top"
                                />
                            </RadioGroup>
                        </FormControl>
                    </Container>
                    <Container>
                        <Grid container spacing={2}>
                            <Grid item xs={12} sm={3}>
                                <FormControl
                                    disabled={tipoBusqueda !== "Cliente"}
                                    fullWidth
                                    error={!Utils.isEmpty(errorCliente)}>
                                    <InputLabel id="select-cliente-label">Cliente</InputLabel>
                                    <Select                                    
                                        labelId="select-cliente-label"
                                        id="demo-simple-select"
                                        value={cliente.id}
                                        onChange={(e) => onChangeCliente(e.target.value)}
                                    >
                                        <MenuItem value="">
                                            <em>Selecciona una opción</em>
                                        </MenuItem>
                                        {clientesItems.map((cliente) => (
                                            <MenuItem key={cliente.id} value={cliente.id}>{cliente.razonSocial}</MenuItem>
                                        ))}
                                    </Select>
                                    <FormHelperText>{errorCliente}</FormHelperText>
                                </FormControl>
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <TextField
                                    disabled={tipoBusqueda !== "NumeroFactura"}
                                    fullWidth
                                    label="Número de Factura"
                                    name="NumeroFactura"
                                    type="number"
                                    helperText={errorNumeroFactura}
                                    error={!Utils.isEmpty(errorNumeroFactura)}
                                    value={numeroFactura}
                                    onChange={(e) => onChangeNumeroFactura(e.target.value)}
                                />
                            </Grid>
                            <Grid item xs={12} sm={3}>
                                <Button type="button" variant="contained" color="primary" onClick={handleSearch}>
                                    Buscar
                                </Button>
                            </Grid>
                            <Grid item xs={12}>
                                <TableContainer component={Paper}>
                                    <Table>
                                        <TableHead>
                                            <TableRow>
                                                <TableCell>Número de factura</TableCell>
                                                <TableCell>Fecha Emisión</TableCell>
                                                <TableCell>Total facturado</TableCell>
                                            </TableRow>
                                        </TableHead>
                                        <TableBody>
                                            {facturas.map((row, index) => (
                                                <TableRow key={index}>
                                                    <TableCell>{row.numeroFactura}</TableCell>
                                                    <TableCell>{Utils.dateToShortDate(row.fechaEmisionFactura)}</TableCell>
                                                    <TableCell>{Utils.formatNumber(row.totalFactura)}</TableCell>                     
                                                </TableRow>
                                            ))}                                            
                                        </TableBody>
                                    </Table>
                                </TableContainer>
                            </Grid>                            
                        </Grid>
                    </Container>
                </Container>
            </Card>
        </div>
    );
}

export default BuscarFactura;
