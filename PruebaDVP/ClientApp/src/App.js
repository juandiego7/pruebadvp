import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import Facturas from './components/Facturas';
import BuscarFactura from './components/BuscarFactura';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Facturas} />
        <Route path='/facturas' component={Facturas} />
        <Route path='/buscarfactura' component={BuscarFactura} />
      </Layout>
    );
  }
}
