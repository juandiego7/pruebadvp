﻿
class Utils {

    static isEmpty = (value) => value === null || value === undefined || value === '';

    static dateToShortDate = (dateString) => {
        const date = new Date(Date.parse(dateString))
        var dd = date.getDate();
        var mm = date.getMonth() + 1; //January is 0!
        var yyyy = date.getFullYear();
        if (dd < 10) { dd = '0' + dd; }
        if (mm < 10) { mm = '0' + mm; }
        return dd + '/' + mm + '/' + yyyy;
    }

    static formatNumber = (number) => {
        const numericValue = parseFloat(number.toString().replace(/,/g, '')) || 0;
        return numericValue.toLocaleString(undefined, { useGrouping: true })
    } 

}

export default Utils;