﻿import { api } from "./api.service";

export const facturasService = {
    createFactura,
    getFacturasPorCliente,
    getFacturasPorNumeroFactura
};

const endpoint = 'facturas/'


function createFactura(factura) {
    return api._post(endpoint, factura);
}

function getFacturasPorCliente(idCliente) {
    return api._get(`${endpoint}cliente/${idCliente}`);
}

function getFacturasPorNumeroFactura(numeroFactura) {
    return api._get(`${endpoint}numeroFactura/${numeroFactura}`);
}

