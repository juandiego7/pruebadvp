﻿export const api = {
  _get,
  _post,
  _delete,
  _put
};

const url = "api/";//process.env.REACT_APP_URL_API;

function _get(path) {
  const requestOptions = {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
    }
    };
  return fetch(`${url}${path}`, requestOptions).then(handleResponse);
}

function _post(path, body) {
  const requestOptions = {
    method: "POST",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  };
  return fetch(`${url}${path}`, requestOptions).then(handleResponse);
}

function _delete(path, body) {
  const requestOptions = {
    method: "DELETE",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  };
  return fetch(`${url}${path}`, requestOptions).then(handleResponse);
}

function _put(path, body) {
  const requestOptions = {
    method: "PUT",
    body: JSON.stringify(body),
    headers: {
      "Content-Type": "application/json"
    }
  };
  return fetch(`${url}${path}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
  return response.text().then(text => {
    const data = text && JSON.parse(text);
    if (!response.ok) {
      if (response.status === 401) {
        window.location.reload(true);
      }

      const error = (data && data.message) || response.statusText;
      return Promise.reject(error);
    }

    return data;
  });
}
