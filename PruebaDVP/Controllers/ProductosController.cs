using System.Collections.Generic;
using Application.DTOs;
using Application.Interfaces.Services;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace PruebaDVP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductosController : ControllerBase
    {
        private readonly IProductoService _service;

        public ProductosController(IProductoService service)
        {
            _service = service;
        }

        [HttpGet()]
        public Response<IEnumerable<ProductoDto>> GetItems()
        {
            var items = _service.GetProductos();
            return items;
        }
    }
}