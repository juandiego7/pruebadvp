using System.Collections.Generic;
using Application.DTOs;
using Application.Interfaces.Services;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace PruebaDVP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FacturasController : ControllerBase
    {
        private readonly IFacturaService _service;

        public FacturasController(IFacturaService service)
        {
            _service = service;
        }

        [HttpPost]
        public Response<FacturaDto> Create(FacturaDto factura)
        {
            var result = _service.Create(factura);
            return result;
        }

        [HttpGet("cliente/{idCliente}")]
        public Response<IEnumerable<FacturaDto>> GetFacturasPorCliente([FromRoute(Name = "idCliente")] int idCliente)
        {
            var result = _service.GetFacturasPorCliente(idCliente);
            return result;
        }

        [HttpGet("numeroFactura/{numeroFactura}")]
        public Response<IEnumerable<FacturaDto>> GetFacturasPorNumeroFactura([FromRoute(Name = "numeroFactura")] int numeroFactura)
        {
            var result = _service.GetFacturasPorNumeroFactura( numeroFactura);
            return result;
        }
    }
}