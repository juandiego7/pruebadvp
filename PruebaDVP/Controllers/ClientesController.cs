using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Application.DTOs;
using Application.Interfaces.Services;
using Application.Wrappers;
using Microsoft.AspNetCore.Mvc;

namespace PruebaDVP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientesController : ControllerBase
    {
        private readonly IClienteService _service;

        public ClientesController(IClienteService clienteService)
        {
            _service = clienteService;
        }

        [HttpGet("items")]
        public Response<IEnumerable<ClienteItemDto>> GetItems()
        {
            var items = _service.GetItems();
            return items;
        }        
    }
}