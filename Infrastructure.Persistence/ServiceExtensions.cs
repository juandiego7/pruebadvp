﻿using Application.Interfaces.Repositories;
using Infrastructure.Persistence.Repositorios;
using Microsoft.Extensions.DependencyInjection;

namespace Infrastructure.Persistence
{
    public static class ServiceExtensions
    {
        public static void AddPersistenceInfraestructure(this IServiceCollection services)
        {
            #region Repositories
            services.AddTransient<IClienteRepository, ClienteRepository>();
            services.AddTransient<IProductoRepository, ProductoRepository>();
            services.AddTransient<IFacturaRepository, FacturaRepository>();
            #endregion
        }
    }
}

