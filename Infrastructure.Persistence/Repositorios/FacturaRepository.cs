﻿using System;
using Application.Interfaces.Repositories;
using Domain.Entities;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;

namespace Infrastructure.Persistence.Repositorios
{
    public class FacturaRepository : IFacturaRepository
    {
        private readonly string connectionString;

        public FacturaRepository(IOptions<AppSettings> config)
        {
            this.connectionString = config.Value.DefaultConnection;
        }

        public TblFactura Create(TblFactura factura)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("sp_InsertFacturaConDetalles", connection))
                    {
                        string facturaJson = JsonConvert.SerializeObject(factura);
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@JsonFactura", SqlDbType.NVarChar)).Value = facturaJson;
                        int nuevoID = Convert.ToInt32(command.ExecuteScalar());
                        factura.Id = nuevoID;
                        return factura;
                    }
                }
            }
            catch (SqlException ex)
            {
                if(ex.Number == 2627)
                {
                    throw new Exception("El número de factura " + factura.NumeroFactura + " ya existe");
                }
                throw new Exception("Error inesperado");
            }
            catch(Exception)
            {
                throw new Exception("Error inesperado");
            }
        }

        public IEnumerable<TblFactura> GetFacturasPorCliente(int idCliente)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("sp_SelectFacturasPorCliente", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@IdCliente", SqlDbType.Int)).Value = idCliente;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            var entities = new List<TblFactura>();
                            while (reader.Read())
                            {
                                int numero = (int)reader["NumeroFactura"];
                                DateTime fechaEmisionFactura = (DateTime)reader["FechaEmisionFactura"];
                                decimal totalFactura = (decimal)reader["TotalFactura"];

                                entities.Add(new TblFactura
                                {
                                    NumeroFactura = numero,
                                    FechaEmisionFactura = fechaEmisionFactura,
                                    TotalFactura = totalFactura
                                });
                            }
                            return entities;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Error inesperado");
            }
        }

        public IEnumerable<TblFactura> GetFacturasPorNumeroFactura(int numeroFactura)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    connection.Open();
                    using (SqlCommand command = new SqlCommand("sp_SelectFacturasPorNumeroFactura", connection))
                    {
                        command.CommandType = CommandType.StoredProcedure;
                        command.Parameters.Add(new SqlParameter("@NumeroFactura", SqlDbType.Int)).Value = numeroFactura;

                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            var entities = new List<TblFactura>();
                            while (reader.Read())
                            {
                                int numero = (int)reader["NumeroFactura"];
                                DateTime fechaEmisionFactura = (DateTime)reader["FechaEmisionFactura"];
                                decimal totalFactura = (decimal)reader["TotalFactura"];

                                entities.Add(new TblFactura
                                {
                                    NumeroFactura = numero,
                                    FechaEmisionFactura = fechaEmisionFactura,
                                    TotalFactura = totalFactura
                                });
                            }
                            return entities;
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw new Exception("Error inesperado");
            }
        }
    }
}