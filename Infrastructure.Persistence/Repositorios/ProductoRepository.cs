﻿using System;
using Application.Interfaces.Repositories;
using Domain.Entities;
using Microsoft.Extensions.Options;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;

namespace Infrastructure.Persistence.Repositorios
{
    public class ProductoRepository : IProductoRepository
    {
        private readonly string connectionString;

        public ProductoRepository(IOptions<AppSettings> config)
        {
            this.connectionString = config.Value.DefaultConnection;
        }

        public IEnumerable<CatProducto> GetProductos()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("sp_GetAllProductos", connection))
                {

                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var entities = new List<CatProducto>();
                        while (reader.Read())
                        {
                            int id = (int)reader["Id"];
                            string nombreProducto = reader["NombreProducto"].ToString();
                            byte[] imagenProducto = new byte[0];
                            if (!reader.IsDBNull(reader.GetOrdinal("ImagenProducto")))
                            {
                                imagenProducto = (byte[])reader["ImagenProducto"];
                            }

                            decimal precioUnitario = Convert.ToDecimal(reader["PrecioUnitario"]);

                            entities.Add(new CatProducto
                            {
                                Id = id,
                                NombreProducto = nombreProducto,
                                ImagenProducto = imagenProducto,
                                PrecioUnitario = precioUnitario
                            });
                        }
                        return entities;
                    }
                }
            }
        }        
    }
}

