﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using Application.Interfaces.Repositories;
using Domain.Entities;
using Microsoft.Extensions.Options;

namespace Infrastructure.Persistence.Repositorios
{
	public class ClienteRepository : IClienteRepository
    {
        private readonly string connectionString;

        public ClienteRepository(IOptions<AppSettings> config)
        {
            this.connectionString = config.Value.DefaultConnection;
        }

        public IEnumerable<TblCliente> GetItems()
        {
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();
                using (SqlCommand command = new SqlCommand("sp_GetAllClientesItems", connection))
                {
                    command.CommandType = CommandType.StoredProcedure;

                    using (SqlDataReader reader = command.ExecuteReader())
                    {
                        var entities = new List<TblCliente>();
                        while (reader.Read())
                        {
                            int id = (int)reader["Id"];
                            string razonSocial = reader["RazonSocial"].ToString();
                            entities.Add(new TblCliente
                            {
                                Id = id,
                                RazonSocial = razonSocial
                            });
                        }
                        return entities;
                    }
                }
            }
        }        
    }
}

