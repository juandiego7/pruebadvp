﻿using System;
namespace Application.DTOs
{
	public class DetalleFacturaDto
	{
        public int Id { get; set; }
        public int IdFactura { get; set; }
        public int IdProducto { get; set; }
        public int CantidadDeProducto { get; set; }
        public decimal PrecioUnitarioProducto { get; set; }
        public decimal SubTotalProducto { get; set; }
        public string Notas { get; set; }
    }
}

