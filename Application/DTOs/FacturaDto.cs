﻿using System;
using System.Collections.Generic;

namespace Application.DTOs
{
	public class FacturaDto
	{
        public int Id { get; set; }
        public DateTime FechaEmisionFactura { get; set; }
        public int IdCliente { get; set; }
        public int NumeroFactura { get; set; }
        public int NumeroTotalArticulos { get; set; }
        public decimal SubTotalFactura { get; set; }
        public decimal TotalImpuesto { get; set; }
        public decimal TotalFactura { get; set; }

        public IEnumerable<DetalleFacturaDto> DetallesFactura { get; set; }
    }
}

