﻿using System.Collections.Generic;
using Application.DTOs;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Wrappers;
using AutoMapper;
namespace Application.Services
{
	public class ClienteService : IClienteService
    {
        private readonly IClienteRepository _repository;
        private readonly IMapper _mapper;

        public ClienteService(IClienteRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<IEnumerable<ClienteItemDto>> GetItems()
        {
            var result = _repository.GetItems();
            var clients = _mapper.Map<IEnumerable<ClienteItemDto>>(result);
            return new Response<IEnumerable<ClienteItemDto>>(clients);
        }
    }
}

