﻿using System;
using Application.DTOs;
using Application.Interfaces.Repositories;
using Application.Interfaces.Services;
using Application.Wrappers;
using AutoMapper;
using System.Collections.Generic;
using Domain.Entities;

namespace Application.Services
{
	public class FacturaService : IFacturaService
    {
        private readonly IFacturaRepository _repository;
        private readonly IMapper _mapper;

        public FacturaService(IFacturaRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<FacturaDto> Create(FacturaDto factura)
        {
            try
            {
                var mapFactura = _mapper.Map<TblFactura>(factura);
                var result = _repository.Create(mapFactura);
                var facturaResult = _mapper.Map<FacturaDto>(result);
                return new Response<FacturaDto>(facturaResult);
            }
            catch (Exception ex)
            {
                return new Response<FacturaDto>(ex.Message);
            }            
        }

        public Response<IEnumerable<FacturaDto>> GetFacturasPorCliente(int idCliente)
        {
            try
            {
                var result = _repository.GetFacturasPorCliente(idCliente);
                var facturas = _mapper.Map<IEnumerable<FacturaDto>>(result);
                return new Response<IEnumerable<FacturaDto>>(facturas);
            }
            catch (Exception ex)
            {
                return new Response<IEnumerable<FacturaDto>>(ex.Message);
            }
        }

        public Response<IEnumerable<FacturaDto>> GetFacturasPorNumeroFactura( int numeroFactura)
        {
            try
            {
                var result = _repository.GetFacturasPorNumeroFactura(numeroFactura);
                var facturas = _mapper.Map<IEnumerable<FacturaDto>>(result);
                return new Response<IEnumerable<FacturaDto>>(facturas);
            }
            catch (Exception ex)
            {
                return new Response<IEnumerable<FacturaDto>>(ex.Message);
            }
        }
    }
}

