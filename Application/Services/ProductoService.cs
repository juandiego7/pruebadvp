﻿using System;
using Application.DTOs;
using Application.Interfaces.Repositories;
using Application.Wrappers;
using AutoMapper;
using System.Collections.Generic;
using Application.Interfaces.Services;

namespace Application.Services
{
    public class ProductoService : IProductoService
    {
        private readonly IProductoRepository _repository;
        private readonly IMapper _mapper;

        public ProductoService(IProductoRepository repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        public Response<IEnumerable<ProductoDto>> GetProductos()
        {
            var result = _repository.GetProductos();
            var productos = _mapper.Map<IEnumerable<ProductoDto>>(result);
            return new Response<IEnumerable<ProductoDto>>(productos);
        }
    }
}

