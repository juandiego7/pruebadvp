﻿using Application.DTOs;
using AutoMapper;
using Domain.Entities;

namespace Application.Mappings
{
	public class GeneralProfile : Profile
    {
        public GeneralProfile()
        {
            CreateMap<CatProducto, ProductoDto>().ReverseMap();
            CreateMap<TblCliente, ClienteItemDto>().ReverseMap();
            CreateMap<TblFactura, FacturaDto>().ReverseMap();
            CreateMap<TblDetalleFactura, DetalleFacturaDto>().ReverseMap();
        }
    }
}
