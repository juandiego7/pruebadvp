﻿using System.Collections.Generic;
using Domain.Entities;

namespace Application.Interfaces.Repositories
{
	public interface IClienteRepository
    { 
        IEnumerable<TblCliente> GetItems();
    }
}

