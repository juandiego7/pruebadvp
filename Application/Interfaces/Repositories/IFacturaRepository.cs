﻿using System.Collections.Generic;
using Domain.Entities;

namespace Application.Interfaces.Repositories
{
	public interface IFacturaRepository 
    {
        TblFactura Create(TblFactura factura);
        IEnumerable<TblFactura> GetFacturasPorCliente(int idCliente);
        IEnumerable<TblFactura> GetFacturasPorNumeroFactura(int numeroFactura);
    }
}

