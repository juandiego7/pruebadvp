﻿using System.Collections.Generic;
using Domain.Entities;

namespace Application.Interfaces.Repositories
{
	public interface IProductoRepository
    {
        IEnumerable<CatProducto> GetProductos();
    }
}

