﻿using Application.DTOs;
using Application.Wrappers;
using System.Collections.Generic;

namespace Application.Interfaces.Services
{
	public interface IClienteService
	{
        Response<IEnumerable<ClienteItemDto>> GetItems();
    }
}

