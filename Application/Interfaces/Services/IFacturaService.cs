﻿using System.Collections.Generic;
using Application.DTOs;
using Application.Wrappers;

namespace Application.Interfaces.Services
{
	public interface IFacturaService
	{
        Response<FacturaDto> Create(FacturaDto factura);
        Response<IEnumerable<FacturaDto>> GetFacturasPorCliente(int idCliente);
        Response<IEnumerable<FacturaDto>> GetFacturasPorNumeroFactura(int numeroFactura);
    }
}

