﻿using Application.DTOs;
using Application.Wrappers;
using System.Collections.Generic;

namespace Application.Interfaces.Services
{
	public interface IProductoService
	{
        Response<IEnumerable<ProductoDto>> GetProductos();
    }
}

